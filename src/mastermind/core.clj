(ns mastermind.core
    (:use midje.sweet)
    (:gen-class))
;;  ## [VARIABLES]
;;  Basic main pawn colors of Mastermind (6 différents colors)
(def initGuessPawn
  {:y "Yellow", :b "Blue", :r "Red", :p "Purple", :g "Green", :o "Orange"})

;;  Basic main clue colors of Mastermind (2 differents colors)
;;  Red   : One for each good position and good color at the same time
;;  White : For each good colors which is not already Red (good pos & good color).
(def initCluePawn
  {:r "Red", :w "White"})

;; max turns
(def max-turn 10)
;; Pattern to find to win
(def initWinningPattern ["y" "b" "r" "b"])

;; ## [FUNCTIONS]
;; Return the color of the pattern : color[patt[pos]]
;; @patt pattern input
;; @pos position in the pattern
;; @colorhm hashmap of colors
(defn getColor [patt pos colorhm]
  ((keyword (patt pos)) colorhm)
  )
;; Return the color of the pattern : color[val]
;; @val the key of colorhm
;; @colorhm hashmap of colors
(defn getColor [val colorhm]
  ((keyword val) colorhm)
  )

;; Return a sorted vec with no ''
;; @vec a sorted vec ("" are in first place)
(defn removeVoidInArray [vec]
  (def i 0)
  (while (and
          (< i (count vec))
          (= (get vec i) "")
          )
         (do
           (def i (+ i 1))
           )
         )
  (subvec vec i)
  )

(defn existColorInPattern [pattern color]
  (def exist false)
  (doseq [patternColor pattern] (
      if (= patternColor color)
      (def exist true)
      )
    )
  (true? exist)
  )
;; Print a pattern with full color name : [ Orange ] instead of [ o ]
;; @patt pattern of diminished color name
;; @colorhm hashmap of colors
(defn printPatt [patt colorhm]
  (doseq [c patt]
    (print (str "[ " ((keyword c) colorhm)) "] " )
    )
  (print "\n")
  )


;; Get the next possible color from one color. Take the same order than the listColor
;; @color the actual color
;; listColor a hashmap (parameter name wrong)
(defn getNextColor [color listColor]
  (def l (vec (keys listColor)))
  (def res  "")
  (def index)
  (doseq [i (range (count l))]
    (do
      (def n (name (get l i)))
      (if (= n color )
      (def index (+ i 1))
      ))
    )
  (if (= index (count l))
    (def index 0)
    )
  (def res (name (get l index)))
  (str res)
  )

;; Return a random vector of colors
;; @colors hashmap of colors.
(defn getRandom [colors]
  (def listColor (vec(keys colors)))
  (def size (count colors ))
  (def c1  (name (get listColor (rand-int size))))
  (def c2  (getNextColor c1 colors))
  (vec[c1 c1 c2 c2])
  )

;; Return a random vector of colors, taking in consideration the ban list.
;; @colors hashmap of colors
;; @ban vector of banned color
(defn getRandomWithBan [colors ban]
  (def colorsKeys (keys colors))
  (if (= nil ban)
    (do
      (getRandom colors))
    (do
      (def newColors {})
      (doseq [key colorsKeys]
        (do (def c (name key)) (def existColorInBan false)
          (doseq [b ban] (if (= c b) (def existColorInBan true)))
          (if (not existColorInBan)
            (do
              (def newColors (assoc newColors key ""))) ;; "" has no importance
            )
          )
        )
      (getRandom newColors)
      ))
  )

;; Return a list of 4 clues
(defn giveOneClue [winningPatt guessedColor pos colors]
  ;; lot of definitions...
  (def winningColor (get winningPatt pos))
  (def cluesColors (vec (keys colors)))
  (def colorNothing              "")
  (def colorGoodColorAndGoodPos (name(get cluesColors 0)))
  (def colorGoodColor           (name(get cluesColors 1)))
  ;; Value of actual color will change if it doesn't match conditions
  (def res colorNothing)
  ;; pos good & good color?
  (if (= winningColor guessedColor)
    ;;return goodcolor & goodpos color
    (def res colorGoodColorAndGoodPos)
    ;;else if good color?
    (if (existColorInPattern winningPatt guessedColor)
      ;;return good color
      (def res colorGoodColor)
      )
    )
  (str res)
  )

(defn get2 [col pos]
  (def res (get col pos))
  (if (= res nil) 0 res)
  )

(defn swap [vec x1 x2]
  (def tmp (get vec x1))
  (def res (assoc vec x1 (get vec x2)))
  (assoc res x2 tmp)
  )

(defn swapNext [v valid]
  (def x1 -1)
  (def x2 -1)
  (doseq [i (range (count valid))]
    (if (not (= "v" (get valid i)))
      (if(= -1 x1)
        (def x1 i)
        (if (= -1 x2)
          (def x2 i)
          )
        )
      )
    )
  (if (and (> x1 -1)(> x2 -1))
    (swap v x1 x2)
    (vec v)
    )
  )

;; Play a turn... Main algorithm.
(defn play [turn guess colors last-clues-1 last-clues-2 last-guess-1 last-guess-2 ban valid]
  (def res {:ban ban, :valid valid, :guess guess}) ;; todo : for any reason it doesn't keep content (all becomes nil)
  (if (= turn 0)
    (assoc res :guess (getRandom colors))
    ( do
      ;; --------- Resolver
      ;; Init clues
      (def c-1 (removeVoidInArray last-clues-1))
      (def c-2 (removeVoidInArray last-clues-2))
      (def freqC-1 (frequencies c-1)) ;; Frequencies bugs sometime (gives "" key instead of "r" or "w")
      (def freqC-2 (frequencies c-2)) ;; Same thing
      ;;(println "C-1 : " c-1)      (println "fC-1 : " freqC-1)      (println "C-2 : " c-2)      (println "fC-2 : " freqC-2)      (println "before ban : " (:ban res))      (println "guess      : " guess)      (println "freqc-1 size : " (count freqC-1))
      ;; If no clues == all colors on this turn banned
      (if (= 0 (count freqC-1))
        (do
          (def res (assoc res :ban (vec (distinct  (concat (:ban res) guess)  ))))
          (def res (assoc res :guess (getRandomWithBan colors (:ban res))))
          ;;(assoc res :guess (getRandomWithBan colors (:ban res)))
          )
        ;; Else
        (do
          (def g guess)
          (def freqG-1 (frequencies last-guess-1))
          (def freqG-2 (frequencies last-guess-2))
          (def freqValid (frequencies valid))
          ;; IF number RED > valid one
          (if (> (get2 freqC-1 :r) (get2 valid :v))
            ;; IF number valid <= 2
            (if (< 3 (get2 freqValid :v))
              (def res (assoc res :guess (swapNext guess valid)))
               ;;todo
              )
            )
          ;;(println "concat : " (distinct (concat (:ban res) guess)))      (println "BANS : " (:ban res)) (println "RES  : " res)
          ;; --------- \ Resolver
          )
        )
      )
    )
  res ;; Issue content not kept
  )
;; Give clues.
;; First color = Good position && Good color
;; Second color = (if not first case) Good color
(defn giveAllClues [winningPatt guess colors]
  (def wpTmp winningPatt)
  (def res "")
  (vec(sort (fn [a b ] (compare b a))
            (vec(for [index (range 4)]
                  (do
                    (def res (giveOneClue wpTmp (get guess index) index colors))
                    (if (not(= res "")) (def wpTmp (assoc wpTmp index "")))
                    (str res)
                    )
                  )
                )
            ))
   )

;; ## [Main Function & global var]
(defn -main
  ""
  [& args]
  ;;  Initialization of colors
  (def color-guess initGuessPawn)
  (def color-clues  initCluePawn)
  (def color-clues-keys (vec(keys color-clues)))
  (def clue1 (name (color-clues-keys 0)))
  (def clue2 (name (color-clues-keys 1)))
  ;;  Initialization of mastermind's board
  (def winning-pattern initWinningPattern)
  (def guess ["" "" "" ""])
  (def clues ["" "" "" ""])

  ;;  Initialization of mastermind's rules
  (def win false)
  (def turn 0)

  ;; Last move
  (def last-guess-1 ["" "" "" ""]) ;; guess 1 turn before
  (def last-guess-2 ["" "" "" ""]) ;; guess 2 turn before
  (def last-clues-1 ["" "" "" ""]) ;; clues 1 turn before
  (def last-clues-2 ["" "" "" ""]) ;; clues 2 turn before
  (def valid []) ;; valid guesses
  (def ban []) ;; banned color

  ;;  Mastermind game begins...
  (println "================([ M A S T E R M I N D ])================ ")
  (println " ~ GOOD POSITION AND GOOD COLOR : " (getColor clue1 color-clues))
  (println " ~ GOOD COLOR ONLY              : " (getColor clue2 color-clues))
  (println "#################### Winning Pattern ####################")
  (print "       ")
  (printPatt winning-pattern color-guess)
  (println "#########################################################")
  (println "################## -[. Game Begins .]- ##################")

  (while (and (< turn max-turn) (not win))
         (println "--------------------------------------  TURN : " turn)
         (println ".........................................................")
         (print "TO GET   : ")
         (printPatt winning-pattern color-guess)
         (print " GUESSING : ")
         (def last-guess-2 last-guess-1)
         (def last-guess-1 guess)
         (def last-clues-2 last-clues-1)
         (def last-clues-1 clues)

         ;; Playing a turn...
         (def answer (play turn guess color-guess last-clues-1 last-clues-2 last-guess-1 last-guess-2 ban valid))

         (def guess (:guess answer))
         (def ban   (:ban answer))
         (def valid (:valid answer))
         (printPatt guess color-guess) ;; print

         (print "    CLUES : ")

         ;; Getting clues ...
         (def clues (giveAllClues winning-pattern guess color-clues))

         (printPatt clues color-clues) ;; print
         (print "COLOR BAN : ")
         (printPatt ban color-guess) ;; print
         (print "    VALID : ")
         (println valid) ;; print

         ;; Check if win
         (def tmpwin true)
         (doseq [c clues]
           (if (not (= c clue1))
             (do
               (def tmpwin false)
               )
             )
           )
         (if tmpwin
           (do
             (println "WIN!")
             (def win true)))
         ;; End of turn
         (def turn (+ turn 1))
         (println "#########################################################")
    )
  )
